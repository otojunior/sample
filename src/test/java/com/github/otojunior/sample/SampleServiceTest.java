/**
 * 
 */
package com.github.otojunior.sample;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Teste de unidade da classe {@link SampleService}
 * @author Oto Soares Coelho Junior
 * @since 27/05/2023
 */
@DisplayName("Serviço de Exemplo")
class SampleServiceTest {
    private SampleService service;

    /**
     * 
     */
    @BeforeEach
    void setup() {
        service = new SampleService();
    }
    
    /**
     * Test method for {@link com.github.otojunior.sample.SampleService#somar(int, int)}.
     */
    @Test
    @DisplayName("Soma de dois operandos")
    void testSomar() {
        assertEquals(8, service.somar(5, 3));
    }

    /**
     * Test method for {@link com.github.otojunior.sample.SampleService#subtrair(int, int)}.
     */
    @Test
    @DisplayName("Subtração de dois operandos")
    void testSubtrair() {
        assertEquals(2, service.subtrair(5, 3));
    }

    /**
     * Test method for {@link com.github.otojunior.sample.SampleService#multiplicar(int, int)}.
     */
    @Test
    @DisplayName("Multiplicação de dois operandos")
    void testMultiplicar() {
        assertEquals(15, service.multiplicar(5, 3));
    }

    /**
     * Test method for {@link com.github.otojunior.sample.SampleService#dividir(int, int)}.
     */
    @Test
    @DisplayName("Divisão de dois operandos")
    void testDividir() {
        assertEquals(5, service.dividir(15, 3));
    }
    
    /**
     * Test method for {@link com.github.otojunior.sample.SampleService#dividir(int, int)}.
     */
    @Test
    @DisplayName("Divisão de operando por zero")
    void testDividirPorZero() {
        assertThrows(
            IllegalArgumentException.class,
            () -> assertEquals(5, service.dividir(15, 0)));
    }
}
