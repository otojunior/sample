/**
 * 
 */
package com.github.otojunior.sample;

/**
 * Sample Main
 * @author Oto Soares Coelho Junior
 * @since 27/05/2023
 */
public class Sample {
    /**
     * @param args
     */
    public static void main(String[] args) {
        var service = new SampleService();
        System.out.println("---------- SAMPLE SERVICE ----------");
        System.out.println("Soma (5,3): " + service.somar(5, 3));
        System.out.println("Subtração (5,3): " + service.subtrair(5, 3));
        System.out.println("Multiplicação (5,3): " + service.multiplicar(5, 3));
        System.out.println("Divisão (15,3): " + service.dividir(15, 3));
        System.out.println("------------------------------------");
    }
}
