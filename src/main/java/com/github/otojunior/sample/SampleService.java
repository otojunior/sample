/**
 * 
 */
package com.github.otojunior.sample;

/**
 * Sample Service
 * @author Oto Soares Coelho Junior
 * @since 27/05/2023
 */
public class SampleService {
    /**
     * Soma dois operandos
     * @param operando1 Operando 1
     * @param operando2 Operando 2
     * @return Resultado da soma dos operandos.
     */
    public int somar(int operando1, int operando2) {
        return operando1 + operando2;
    }

    /**
     * Subtrai dois operandos
     * @param operando1 Operando 1
     * @param operando2 Operando 2
     * @return Resultado da subtração dos operandos.
     */
    public int subtrair(int operando1, int operando2) {
        return operando1 - operando2;
    }

    /**
     * Multiplica dois operandos
     * @param operando1 Operando 1
     * @param operando2 Operando 2
     * @return Resultado da multiplicação dos operandos.
     */
    public int multiplicar(int operando1, int operando2) {
        return operando1 * operando2;
    }

    /**
     * Divide dois operandos
     * @param operando1 Operando 1
     * @param operando2 Operando 2
     * @return Resultado da divisão dos operandos.
     */
    public double dividir(int operando1, int operando2) {
        if (operando2 != 0) {
            var double1 = (double)operando1;
            var double2 = (double)operando2;
            return double1 / double2;
        } else {
            throw new IllegalArgumentException("Operando 2 não pode ser 0");
        }
    }
}
